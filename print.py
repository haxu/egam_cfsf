from ROOT import *
import sys
from array import array
import math
import numpy as np

def main(argv):
    gROOT.LoadMacro("AtlasStyle.C")
    WP = str(argv[0])
#    gStyle.SetOptStat(False)
    period = str(argv[1])
    inputF = TFile("TruthRate"+period+".root","READ")
    pW = inputF.Get("CFT"+WP+"_plus_Wrong")
    mW = inputF.Get("CFT"+WP+"_minus_Wrong")
    pT = inputF.Get("CFT"+WP+"_plus_Total")
    mT = inputF.Get("CFT"+WP+"_minus_Total")
    fulleta = [-2.5,-2.3,-1.7,-1.52,-1.25,-0.75,0.0, 0.75, 1.25, 1.52, 1.7, 2.3, 2.5]
    etaArr=array('d',fulleta)
    
    nEta=len(fulleta) - 1
    hp = TH1F("hp","hp",nEta, etaArr)
    hm = TH1F("hm","hm",nEta, etaArr)
    for i in range(1,nEta+1):
        Rp = pW.GetBinContent(i,1)/pT.GetBinContent(i,1)
        Rm = mW.GetBinContent(i,1)/mT.GetBinContent(i,1)
        hp.SetBinContent(i,Rp)
        hm.SetBinContent(i,Rm)
    c1 = TCanvas("","",800,600)
    hm.SetLineColor(kRed)
    hp.Draw("hist")
    hm.Draw("histsame")
    c1.Print("~/EgammaOutput/"+WP+"_"+period+".pdf")
    
    #gStyle.SetOptStat(True)
    hp.Draw("hist")
    c1.Print("~/EgammaOutput/"+WP+"_"+period+"_plus.pdf")
    hm.Draw("hist")
    c1.Print("~/EgammaOutput/"+WP+"_"+period+"_minus.pdf")
    hp.Add(hm,-1)
    hp.Divide(hm)
    hp.Draw("hist")
    c1.Print("~/EgammaOutput/"+WP+"_"+period+"_Syst.pdf")
    return

main(sys.argv[1:])
