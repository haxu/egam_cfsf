from ROOT import *
#from ROOT import TH2F, TH1F, TFile,TTree
from array import array
import math
import numpy as np
import sys
def ApplyPreCuts(tree):
   if not tree.isTagTag==1:
      return False
   if not tree.isMultiElectronTriggerMatched:
      return False
   if tree.elCand1_pt<20000. or tree.elCand2_pt<20000.:
      return False
   if math.fabs(tree.elCand1_cl_eta)>1.37 and math.fabs(tree.elCand1_cl_eta)<1.52:
      return False
   if math.fabs(tree.elCand2_cl_eta)>1.37 and math.fabs(tree.elCand2_cl_eta)<1.52:
      return False
   if not tree.elCand2_FirstEgMotherOrigin==13:
      return False
   if not tree.elCand1_FirstEgMotherOrigin==13:
      return False
   if not tree.elCand2_FirstEgMotherTyp==2:
      return False
   if not tree.elCand1_FirstEgMotherTyp==2:
      return False
   if math.fabs(tree.elCand2_cl_eta)>2.47 or math.fabs(tree.elCand1_cl_eta)>2.47:
      return False
   if tree.Zcand_M<80000. or tree.Zcand_M>100000.:
     return False
   return True

def ApplyWPcutsTightT(tree):
    if tree.elCand1_FixedCutTight==0 or tree.elCand2_FixedCutTight==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
def ApplyWPcutsTightTCFT(tree):
    if tree.elCand1_FixedCutTight==0 or tree.elCand2_FixedCutTight==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_passCFTloose==0 or tree.elCand2_passCFTloose==0:
      return False
    else:
      return True


def ApplyWPcutsTightT(tree):
    if tree.elCand1_FixedCutTight==0 or tree.elCand2_FixedCutTight==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
def ApplyWPcutsTightGCFT(tree):
    if tree.elCand1_isolGradient==0 or tree.elCand2_isolGradient==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_passCFTloose==0 or tree.elCand2_passCFTloose==0:
      return False
    else:
      return True
def ApplyWPcutsTight(tree):
    if tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True

def ApplyWPcutsTightL(tree):
    if tree.elCand1_FixedCutLoose==0 or tree.elCand2_FixedCutLoose==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
def ApplyWPcutsTightG(tree):
    if tree.elCand1_isolGradient==0 or tree.elCand2_isolGradient==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
	  
def ApplyWPcutsTightH(tree):
    if tree.elCand1_FixedCutHighPtCaloOnly==0 or tree.elCand2_FixedCutHighPtCaloOnly==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
	  
def ApplyWPcutsMediumT(tree):
    if tree.elCand1_FixedCutTight==0 or tree.elCand2_FixedCutTight==0 or tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
def ApplyWPcutsMediumTCFT(tree):
    if tree.elCand1_FixedCutTight==0 or tree.elCand2_FixedCutTight==0 or tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_passCFTloose==0 or tree.elCand2_passCFTloose==0:
      return False
    else:
      return True

def ApplyWPcutsMediumGCFT(tree):
    if tree.elCand1_isolGradient==0 or tree.elCand2_isolGradient==0 or tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_passCFTloose==0 or tree.elCand2_passCFTloose==0:
      return False
    else:
      return True
def ApplyWPcutsMediumG(tree):
    if tree.elCand1_isolGradient==0 or tree.elCand2_isolGradient==0 or tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
	  
def ApplyWPcutsMedium(tree):
    if tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True

def ApplyWPcutsMediumL(tree):
    if tree.elCand1_FixedCutLoose==0 or tree.elCand2_FixedCutLoose==0 or tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True

def ApplyWPcutsMediumH(tree):
    if tree.elCand1_FixedCutHighPtCaloOnly==0 or tree.elCand2_FixedCutHighPtCaloOnly==0 or tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True	  

def ApplyWPcutsLooseBL(tree):
    if tree.elCand2_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True	  
	  
def ApplyWPcutsLooseBLCFT(tree):
    if tree.elCand2_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_passCFTloose==0 or tree.elCand2_passCFTloose==0:
      return False
    else:
      return True

def ApplyWPcuts(tree,WP): 
        if WP in ["Tight"]: 
	   return ApplyWPcutsTight(tree)
        elif WP in ["CFTTightG"]:
           return ApplyWPcutsTightGCFT(tree)
	elif WP in ["TightT"]:
           return ApplyWPcutsTightT(tree)
	elif WP in ["CFTTightT"]:   
	   return ApplyWPcutsTightTCFT(tree)
	elif WP in ["TightG"]:   
	   return ApplyWPcutsTightG(tree)
	elif WP in ["TightC"]:   
	   return ApplyWPcutsTightH(tree)
	elif WP in ["TightL"]:   
	   return ApplyWPcutsTightL(tree)
	elif WP in ["Medium"]: 
	   return ApplyWPcutsMedium(tree)
	elif WP in ["MediumT"]:   
	   return ApplyWPcutsMediumT(tree)
	elif WP in ["CFTMediumT"]:   
	   return ApplyWPcutsMediumTCFT(tree)
	elif WP in ["MediumG"]:   
	   return ApplyWPcutsMediumG(tree)
	elif WP in ["MediumC"]:   
	   return ApplyWPcutsMediumH(tree)
	elif WP in ["MediumL"]:   
	   return ApplyWPcutsMediumL(tree)
	elif WP in ["CFTMediumG"]:   
	   return ApplyWPcutsMediumGCFT(tree)
	elif WP in ["LooseB"]:   
	   return ApplyWPcutsLooseBL(tree)
	elif WP in ["CFTLooseB"]:   
	   return ApplyWPcutsLooseBLCFT(tree)   
	else:
	   return False
	   
def main(argv):
    print "python ExtractTruth.py period job_number"
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    gROOT.SetBatch(1)
    gStyle.SetLineWidth(1)
    gStyle.SetMarkerSize(1)
    gStyle.SetOptStat(False)
    gStyle.SetPaintTextFormat("1.4f");
    gStyle.SetErrorX(0)
    etas = [0.0, 0.75, 1.25, 1.52, 1.7, 2.3, 2.5]
    fulleta = [-2.5,-2.3,-1.7,-1.52,-1.25,-0.75,0.0, 0.75, 1.25, 1.52, 1.7, 2.3, 2.5]
    #pts  = [20.0,30.0,40.0,50.0,60.0,70.0, 95.0, 130.0, 1000.0]
    #pts  = [20.0,30.0,35.0,40.0,45.0,60.0,95.0,115, 145, 185, 240,7000.0]
    pts =     [20.0,7000.0]
    Period = argv[0]
    inputpath = "./mc"+str(Period)+".root"
    if Period == "1516":
       inputpath = "/lustre/stormfs/atlas/atlaslocalgroupdisk/rucio/user/haxu/42/8f/mc1516.root" 
    if Period == "17":
       inputpath = "/lustre/stormfs/atlas/atlaslocalgroupdisk/rucio/user/haxu/67/8b/mc17.root"
    ZeeMC = TFile(str(inputpath),"READ")
    ZeeTree = ZeeMC.Get("ZeeCandidate").Clone()
    etaArr=array('d',fulleta)
    ptArr = array('d',pts)
    nEta=len(fulleta) - 1
    nPt = len(pts) - 1

    Output = TFile("./TruthRate"+str(Period)+"_"+str(argv[1])+".root","recreate")
    Nentry = ZeeTree.GetEntries()
    WorkingPoints=["CFTTightG","CFTMediumG","CFTMediumT","CFTTightT","CFTLooseB"]
    PlusOfHistWrong = []
    MinusOfHistWrong = []
    PlusHistTotal = []
    MinusHistTotal = []
    for index_wp in range(0,5):
            print WorkingPoints[index_wp]
            #HistWrong = TH2F(str(WorkingPoints[index_wp])+"Wrong",";#eta;P_{T}",nEta, etaArr, nPt, ptArr)
            #HistTotal = TH2F(str(WorkingPoints[index_wp])+"Total",";#eta;P_{T}",nEta, etaArr, nPt, ptArr)
	    PlusOfHistWrong.append(TH2F(str(WorkingPoints[index_wp])+"_plus_Wrong",";#eta;P_{T}",nEta, etaArr, nPt, ptArr))
            MinusOfHistWrong.append(TH2F(str(WorkingPoints[index_wp])+"_minus_Wrong",";#eta;P_{T}",nEta, etaArr, nPt, ptArr))
            PlusHistTotal.append(TH2F(str(WorkingPoints[index_wp])+"_plus_Total",";#eta;P_{T}",nEta, etaArr, nPt, ptArr))
            MinusHistTotal.append(TH2F(str(WorkingPoints[index_wp])+"_minus_Total",";#eta;P_{T}",nEta, etaArr, nPt, ptArr))
	
    Start = Nentry*int(argv[1])/100
    End = Nentry*int(int(argv[1])+1)/100
    for index in range(Start,End):
        #continue;
        ZeeTree.GetEntry(index)
        if ZeeTree.Zcand_M>80000. and ZeeTree.Zcand_M<100000.:
           MCPileupWeight  = ZeeTree.MCPileupWeight
        elif ZeeTree.Zcand_M>60000. and ZeeTree.Zcand_M<120000.:
           MCPileupWeight  = 0#-0.5*ZeeTree.MCPileupWeight
        if index%100000 == 0:
           print index 
        if not ApplyPreCuts(ZeeTree):
           continue  
        for index_wp in range(0,5):
	    if not ApplyWPcuts(ZeeTree,str(WorkingPoints[index_wp])):
		   continue;
            
            #if ZeeTree.Zcand_M()<80000. or ZeeTree.Zcand_M()>100000:
            if ZeeTree.elCand1_firstEgMotherPdgId==-11:
               MinusHistTotal[index_wp].Fill(ZeeTree.elCand1_cl_eta,ZeeTree.elCand1_pt/1000.,MCPileupWeight)
            elif ZeeTree.elCand1_firstEgMotherPdgId==11:
               PlusHistTotal[index_wp].Fill(ZeeTree.elCand1_cl_eta,ZeeTree.elCand1_pt/1000.,MCPileupWeight)

            if ZeeTree.elCand2_firstEgMotherPdgId==-11:
	       MinusHistTotal[index_wp].Fill(ZeeTree.elCand2_cl_eta,ZeeTree.elCand2_pt/1000.,MCPileupWeight)
	    elif ZeeTree.elCand2_firstEgMotherPdgId==11:
               PlusHistTotal[index_wp].Fill(ZeeTree.elCand2_cl_eta,ZeeTree.elCand2_pt/1000.,MCPileupWeight)

            if ZeeTree.elCand1_charge*ZeeTree.elCand1_firstEgMotherPdgId>0 :
                if ZeeTree.elCand1_firstEgMotherPdgId==-11:
                   MinusOfHistWrong[index_wp].Fill(ZeeTree.elCand1_cl_eta,ZeeTree.elCand1_pt/1000.,MCPileupWeight)
                elif ZeeTree.elCand1_firstEgMotherPdgId==11:
                   PlusOfHistWrong[index_wp].Fill(ZeeTree.elCand1_cl_eta,ZeeTree.elCand1_pt/1000.,MCPileupWeight)
	        #ListOfHistWrong[index_wp].Fill(math.fabs(ZeeTree.elCand1_cl_eta),ZeeTree.elCand1_pt/1000.,MCPileupWeight)
	    
            if ZeeTree.elCand2_charge*ZeeTree.elCand2_firstEgMotherPdgId>0 :   
	        if ZeeTree.elCand2_firstEgMotherPdgId==-11:
                   MinusOfHistWrong[index_wp].Fill(ZeeTree.elCand2_cl_eta,ZeeTree.elCand1_pt/1000.,MCPileupWeight)
                elif ZeeTree.elCand2_firstEgMotherPdgId==11:
                   PlusOfHistWrong[index_wp].Fill(ZeeTree.elCand2_cl_eta,ZeeTree.elCand1_pt/1000.,MCPileupWeight)
        

    Output.cd()
    for index_wp in range(0,5):
        PlusOfHistWrong[index_wp].Write()
        MinusOfHistWrong[index_wp].Write()
        
	PlusHistTotal[index_wp].Write()
        MinusHistTotal[index_wp].Write()
    #Output.Write()
    #Output.Close()	
    return
main(sys.argv[1:])


