from ROOT import *
#from ROOT import TH2F, TH1F, TFile,TTree
from array import array
import math
#import numpy as np
import sys
TEXTSIZE = 0.035
etas = [0.0, 0.75, 1.25, 1.52, 1.7, 2.3, 2.5]
#pts  = [20.0,30.0,35.0,40.0,45.0,60.0,95.0,115, 145, 185, 240,7000.0]
pts  = [20.0,60.0,90.0,130.0, 200.0]

def GetRate2DHist(File):
    for i in File.GetListOfKeys():
        if i.GetName().find("misid")>0:
           #print i.GetName()
           j = File.Get(str(i.GetName()))
           return j
        else:
          continue
def PrintMat(File):
    for i in File.GetListOfKeys():
        p = i.GetName()
        #print p,p.find("corrMat")
        if p.find("corrMat") > 0:
           gROOT.LoadMacro("AtlasStyle.C")
           SetAtlasStyle()
           gROOT.SetBatch(1)
           gStyle.SetLineWidth(1)
           gStyle.SetMarkerSize(1)
           gStyle.SetOptStat(False)
           gStyle.SetPaintTextFormat("1.2f");

           print i.GetName()
           j = File.Get(str(i.GetName()))
           cs = TCanvas("cs","cs",1000,800)
           cs.SetRightMargin(15)
           
           j.Draw("colz")
           j.GetXaxis().SetRangeUser(0,11)
           j.GetYaxis().SetRangeUser(0,11)
           j.Draw("textsame")
           cs.Print(str(i.GetName())+".pdf")
        else:
          continue

def ReverseWrongToRight(hist2D):
    N_x = int(hist2D.GetNbinsX())
    N_y = int(hist2D.GetNbinsY())
    output2D = hist2D.Clone()
    for in_x in range(0,N_x):
        for in_y in range(0,N_y):
            output2D.SetBinContent(in_x+1,in_y+1,1.-hist2D.GetBinContent(in_x+1,in_y+1))
            #hist1D.SetBinError(in_x*N_y+in_y+1,hist2D.GetBinError(in_x+1,in_y+1))
    return output2D


def Trans2Dto1D(hist2D):
    N_x = int(hist2D.GetNbinsX())
    N_y = int(hist2D.GetNbinsY())
    hist1D = TH1F(str(hist2D.GetName())+"1D",str(hist2D.GetName())+"1D",N_x*N_y,0,N_x*N_y)
    for in_x in range(0,N_x):
        for in_y in range(0,N_y):
            hist1D.SetBinContent(in_x*N_y+in_y+1,hist2D.GetBinContent(in_x+1,in_y+1))
            hist1D.SetBinError(in_x*N_y+in_y+1,hist2D.GetBinError(in_x+1,in_y+1))
    return hist1D


def DrawLine(X):
    line = TLine(int(X),0,int(X),0.2);line.Draw("same");
    return

def DrawTexEta(X,Y,index_bin_eta):
    Latex1 = TLatex(X,Y,"#font[42]{#eta "+str(etas[int(index_bin_eta)])+"-"+str(etas[int(index_bin_eta)+1])+"}");
    Latex1.SetTextSize(TEXTSIZE)
    Latex1.Draw("same")
    return

def DrawHist1D(SF2D_Nominal,SF2D_Truth,SF2D_FSR,SF2D_Sideband,name,Log,YTitle,isDATA=True):
    #disable
    print "disabled"
    return
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    gROOT.SetBatch(1)
    gStyle.SetLineWidth(1)
    gStyle.SetMarkerSize(1)
    gStyle.SetOptStat(False)
    gStyle.SetPaintTextFormat("1.4f");
    #gStyle.SetErrorX(0)
    
    SF1D_Nominal  = Trans2Dto1D(SF2D_Nominal)
    SF1D_Truth  = Trans2Dto1D(SF2D_Truth);SF1D_Truth.SetLineColor(kRed);
    if isDATA:
       SF1D_Truth.SetLineColor(kWhite);
    SF1D_FSR  = Trans2Dto1D(SF2D_FSR);SF1D_FSR.SetLineColor(kBlue);
    SF1D_Sideband  = Trans2Dto1D(SF2D_Sideband);SF1D_Sideband.SetLineColor(kGreen);
    SF1D_Nominal.GetXaxis().SetTitle("p_{T}^{in each #eta bin} [GeV]")
    SF1D_Nominal.GetYaxis().SetTitle(str(YTitle))
    c1 = TCanvas("c1","c1",800,800)
    pad1 = TPad("pad1","pad1",0,0.35,1,1);pad1.SetBottomMargin(0.01); pad1.SetLeftMargin(0.15);pad1.Draw();
    Y_position = 0.3
    if Log:
       pad1.SetLogy();
       SF1D_Nominal.GetYaxis().SetRangeUser(0.00001,10) 
       Y_position = 0.001
    else:
       SF1D_Nominal.GetYaxis().SetRangeUser(0.1,1.9)
       Y_position = 0.3
    pad2 = TPad("pad2","pad2",0,0,1,0.35);pad2.SetTopMargin(0);pad2.SetBottomMargin(0.5);pad2.SetLeftMargin(0.15);pad2.Draw();
    legend = TLegend(0.20,0.65,0.54,0.94);legend.SetBorderSize(0);legend.SetTextFont(42);legend.SetTextSize(TEXTSIZE);legend.SetFillColor(0);legend.SetLineColor(0);
    pad1.cd()
    SF1D_Nominal.Draw("e");SF1D_Truth.Draw("histsame");SF1D_FSR.Draw("histsame");SF1D_Sideband.Draw("histsame");SF1D_Nominal.Draw("esame");
    legend.AddEntry(SF1D_Nominal,"Nominal","apl");
    if not isDATA:
       legend.AddEntry(SF1D_Truth,"Truth (MC)","l"); 
    legend.AddEntry(SF1D_FSR,"FSR","l");legend.AddEntry(SF1D_Sideband,"No-sub","l");
    line = []
    Latex = []
    SF1D_NominalR = SF1D_Nominal.Clone();SF1D_Nominal0 = SF1D_Nominal.Clone();SF1D_TruthR = SF1D_Truth.Clone();SF1D_FSRR = SF1D_FSR.Clone();SF1D_SidebandR = SF1D_Sideband.Clone();
    for i in range(1,7):
        if i <6 :
          line.append(TLine(float(i*11),0,float(i*11),0.2));line[i-1].Draw("same");
        Latex.append(TLatex(float(i*11)-7.5,Y_position,"#font[42]{#eta "+str(etas[int(i-1)])+"-"+str(etas[int(i)])+"}"))
        Latex[i-1].SetTextSize(TEXTSIZE)
        Latex[i-1].Draw("same")
        for j in range(0,11):
            Label = str(pts[j])
            SF1D_NominalR.GetXaxis().SetBinLabel(j+11*i-10,Label)
            
  
    legend.Draw("same");
    pad2.cd()
    #SF1D_NominalR = SF1D_Nominal.Clone();SF1D_Nominal0 = SF1D_Nominal.Clone();SF1D_TruthR = SF1D_Truth.Clone();SF1D_FSRR = SF1D_FSR.Clone();SF1D_SidebandR = SF1D_Sideband.Clone();
    #for index in range(1,int(SF1D_Nominal0.GetNbinsX())+1):
    #    SF1D_Nominal0.SetBinError(index,0)
    SF1D_NominalR.GetYaxis().SetRangeUser(0.5,1.5)
    SF1D_NominalR.GetYaxis().SetTitle("Ratio")
    SF1D_NominalR.Divide(SF1D_Nominal0);SF1D_TruthR.Divide(SF1D_Nominal0);SF1D_FSRR.Divide(SF1D_Nominal0);SF1D_SidebandR.Divide(SF1D_Nominal0)
    SF1D_NominalR.Draw("e");SF1D_TruthR.Draw("histsame");SF1D_FSRR.Draw("histsame");SF1D_SidebandR.Draw("histsame");SF1D_NominalR.Draw("esame");
    c1.Print(str(name))
    pad1.Close();pad2.Close();c1.Close()    
    return 




def main(argv):
    WorkingPoint = argv[1]
    Period = argv[0]
    plotpath = "plots"+str(Period)+"/"
    NoTruthPath = "/home/hanlinxuy/NominalEgamma"#"/pbs/home/h/hxu/MISID/"

#    TruthPath = "/pbs/home/h/hxu/work/CF_check_motiva/"
#    TruthFile = TFile(TruthPath+"TruthRate"+str(Period)+".root","READ")
#    TruthRateW = TruthFile.Get(str(WorkingPoint)+"Wrong")
#    TruthRateT = TruthFile.Get(str(WorkingPoint)+"Total")
 #   TruthRateW.Divide(TruthRateT)
#    TruthRate = TruthRateW.Clone()
#    RTruthRate = ReverseWrongToRight(TruthRate)

    #Nominal Rates with FSR sub and sideband sub 80-100 20
    Data_FSR_bkgsub_File = TFile(NoTruthPath+"/DATA"+str(Period)+"_"+str(WorkingPoint)+"_nominal.root","READ")
    MC_FSR_bkgsub_File = TFile(NoTruthPath+"/MC"+str(Period)+"_"+str(WorkingPoint)+"_nominal.root","READ")
    PrintMat(Data_FSR_bkgsub_File)
    PrintMat(MC_FSR_bkgsub_File)
    ListOfNominalRates = []
    ListOfNominalRates.append(GetRate2DHist(Data_FSR_bkgsub_File))
    #SystTruth0 = GetRate2DHist(Data_FSR_bkgsub_File).Clone()
    ListOfNominalRates.append(GetRate2DHist(MC_FSR_bkgsub_File))
    ListOfNominalRates.append(ReverseWrongToRight(GetRate2DHist(Data_FSR_bkgsub_File)))
    ListOfNominalRates.append(ReverseWrongToRight(GetRate2DHist(MC_FSR_bkgsub_File)))
    #RSystTruth0 = ReverseWrongToRight(GetRate2DHist(Data_FSR_bkgsub_File)).Clone()
  # print ListOfNominalRates[0].GetEntries()

    #FSR Rates with no FSR sub and sideband sub 80-100 20
    Data_noFSR_bkgsub_File = TFile(NoTruthPath+"/DATA"+str(Period)+"_"+str(WorkingPoint)+"_FSR.root","READ")
    MC_noFSR_bkgsub_File = TFile(NoTruthPath+"/MC"+str(Period)+"_"+str(WorkingPoint)+"_FSR.root","READ")
    ListOfFSRRates = []
    ListOfFSRRates.append(GetRate2DHist(Data_noFSR_bkgsub_File))
    ListOfFSRRates.append(GetRate2DHist(MC_noFSR_bkgsub_File))
    ListOfFSRRates.append(ReverseWrongToRight(GetRate2DHist(Data_noFSR_bkgsub_File)))
    ListOfFSRRates.append(ReverseWrongToRight(GetRate2DHist(MC_noFSR_bkgsub_File))) 
    #sideband Rates with  FSR sub and no sideband sub 80-100 20
    Data_FSR_nobkgsub_File = TFile(NoTruthPath+"/DATA"+str(Period)+"_"+str(WorkingPoint)+"_Sideband.root","READ")
    MC_FSR_nobkgsub_File = TFile(NoTruthPath+"/MC"+str(Period)+"_"+str(WorkingPoint)+"_Sideband.root","READ")
    ListOfsidebandRates = []
    ListOfsidebandRates.append(GetRate2DHist(Data_FSR_nobkgsub_File))
    ListOfsidebandRates.append(GetRate2DHist(MC_FSR_nobkgsub_File))
    ListOfsidebandRates.append(ReverseWrongToRight(GetRate2DHist(Data_FSR_nobkgsub_File)))
    ListOfsidebandRates.append(ReverseWrongToRight(GetRate2DHist(MC_FSR_nobkgsub_File)))
#Output Wrong rate and SF
    #DrawHist1D(ListOfNominalRates[0],ListOfFSRRates[0],ListOfFSRRates[0],ListOfsidebandRates[0],plotpath+"DATA1D_"+str(Period)+"_"+str(WorkingPoint)+".pdf",True,"FlipRates",True)
    #DrawHist1D(ListOfNominalRates[1],ListOfFSRRates[1],ListOfFSRRates[1],ListOfsidebandRates[1],plotpath+"MC1D_"+str(Period)+"_"+str(WorkingPoint)+".pdf",True,"FlipRates",True)
  #  DrawHist1D(ListOfNominalRates[0],TruthRate,ListOfFSRRates[0],ListOfsidebandRates[0],"DATA1D_"+str(Period)+"_"+str(WorkingPoint)+".pdf",True,"FlipRates")
    ListOfNominalRates[0].Divide(ListOfNominalRates[1])
    Nominal = ListOfNominalRates[0].Clone()
    ListOfFSRRates[0].Divide(ListOfFSRRates[1])
    ListOfsidebandRates[0].Divide(ListOfsidebandRates[1])
    #SystTruth0.Divide(TruthRate)
    #SystTruth = SystTruth0.Clone()
  
    StatError = ListOfNominalRates[0].Clone()
    SystFSR = ListOfFSRRates[0].Clone()
    SystSideband = ListOfsidebandRates[0].Clone()
    
    #SystTruth.Add(Nominal,-1.)
    SystFSR.Add(Nominal,-1.)
    SystSideband.Add(Nominal,-1.)
    for in_x in range(1,int(StatError.GetNbinsX())+1):
        for in_y in range(1,int(StatError.GetNbinsY())+1): 
            StatError.SetBinContent(in_x,in_y,Nominal.GetBinError(in_x,in_y))
     #       SystTruth.SetBinContent(in_x,in_y,math.fabs(SystTruth.GetBinContent(in_x,in_y)))
            SystSideband.SetBinContent(in_x,in_y,math.fabs(SystSideband.GetBinContent(in_x,in_y)))
            SystFSR.SetBinContent(in_x,in_y,math.fabs(SystFSR.GetBinContent(in_x,in_y)))
    #StatError.Divide(Nominal);SystFSR.Divide(Nominal);SystSideband.Divide(Nominal)
    outputFileWrong = TFile("SF_Wrong_"+str(Period)+"_"+str(WorkingPoint)+".root","recreate")
    outputFileWrong.cd()
    Nominal.SetTitle("Nominal");Nominal.SetName("Nominal");StatError.SetTitle("StatError");StatError.SetName("StatError");
   # SystTruth.SetTitle("SystTruth");SystTruth.SetName("SystTruth");
    SystFSR.SetTitle("SystFSR");SystFSR.SetName("SystFSR");
    SystSideband.SetTitle("SystSideband");SystSideband.SetName("SystSideband");
    StatError.Write();SystFSR.Write();SystSideband.Write();Nominal.Write();#SystTruth.Write();
    outputFileWrong.Close()
    return
  #  DrawHist1D(ListOfNominalRates[0],SystTruth0,ListOfFSRRates[0],ListOfsidebandRates[0],plotpath+"SF1D_"+str(Period)+"_"+str(WorkingPoint)+".pdf",False,"Scale Factor",False)
  #  DrawHist1D(ListOfNominalRates[1],TruthRate,ListOfFSRRates[1],ListOfsidebandRates[1],"MC1D_"+str(Period)+"_"+str(WorkingPoint)+".pdf",True,"FlipRates",False)
    #DrawHist1D(ListOfNominalRates[0],TruthRate,ListOfFSRRates[0],ListOfsidebandRates[0],"DATA1D_"+str(Period)+"_"+str(WorkingPoint)+".pdf",True,"FlipRates")
#Output Right rate and SF    
#    ListOfNominalRates[2].Divide(ListOfNominalRates[3])
#    RNominal = ListOfNominalRates[2].Clone()
#    ListOfFSRRates[2].Divide(ListOfFSRRates[3])
#    ListOfsidebandRates[2].Divide(ListOfsidebandRates[3])
#
#    RStatError = ListOfNominalRates[2].Clone()
#    RSystTruth0.Divide(RTruthRate)
#    RSystTruth = RSystTruth0.Clone()
#    RSystFSR = ListOfFSRRates[2].Clone()
#    RSystSideband = ListOfsidebandRates[2].Clone()
#    #DrawHist1D(ListOfNominalRates[2],ListOfFSRRates[2],ListOfFSRRates[2],ListOfsidebandRates[2],plotpath+"SF1D_"+str(Period)+"_"+str(WorkingPoint)+".pdf",False,"Scale Factor",True) 
#    RSystTruth.Add(RNominal,-1.)
#    RSystFSR.Add(RNominal,-1.)
#    RSystSideband.Add(RNominal,-1.)
#    for in_x in range(1,int(RStatError.GetNbinsX())+1):
#        for in_y in range(1,int(RStatError.GetNbinsY())+1):
#            RStatError.SetBinContent(in_x,in_y,RNominal.GetBinError(in_x,in_y))
#            RSystTruth.SetBinContent(in_x,in_y,math.fabs(RSystTruth.GetBinContent(in_x,in_y)))
#            RSystSideband.SetBinContent(in_x,in_y,math.fabs(RSystSideband.GetBinContent(in_x,in_y)))
#            RSystFSR.SetBinContent(in_x,in_y,math.fabs(RSystFSR.GetBinContent(in_x,in_y)))
#    #RStatError.Divide(RNominal);RSystFSR.Divide(RNominal);RSystSideband.Divide(RNominal)
#   # DrawHist1D(ListOfNominalRates[2],ListOfFSRRates[2],ListOfFSRRates[2],ListOfsidebandRates[2],plotpath+"SF1D_"+str(Period)+"_"+str(WorkingPoint)+".pdf",False,"Scale Factor",True)  
#   
#    RNominal.SetTitle("Nominal");RNominal.SetName("Nominal");RStatError.SetTitle("StatError");RStatError.SetName("StatError");
#    RSystTruth.SetTitle("SystTruth");RSystTruth.SetName("SystTruth");
#    RSystFSR.SetTitle("SystFSR");RSystFSR.SetName("SystFSR");
#    RSystSideband.SetTitle("SystSideband");RSystSideband.SetName("SystSideband");
#    outputFileRight = TFile("SF_Right_"+str(Period)+"_"+str(WorkingPoint)+".root","recreate")
#    outputFileRight.cd()
#    RStatError.Write();RSystFSR.Write();RSystSideband.Write();RNominal.Write();RSystTruth.Write();
#    outputFileRight.Close()
#

#    StatError.Divide(Nominal);SystTruth.Divide(Nominal);SystFSR.Divide(Nominal);SystSideband.Divide(Nominal)

    return
main(sys.argv[1:])
