
from ROOT import *
#from ROOT import TH2F, TH1F, TFile,TTree
from array import array
import math
import numpy as np
import sys

etas = [0.0, 0.75, 1.25, 1.52, 1.7, 2.3, 2.5]
#pts  = [20.0,30.0,35.0,40.0,45.0,60.0,95.0,115, 145, 185, 240,7000.0]
pts  = [20.0,60.0,90.0,130.0, 200.0]
etaArr, ptArr = array('d',etas), array('d',pts)
nEta,nPt = len(etas) - 1, len(pts) - 1
#countinghist = TH2F(histName, ";#eta;P_{T}",nEta, etaArr, nPt, ptArr)

def Nominal(File, histname):
  
    NominalHist = File.Get("Nominal").Clone();
    outputhist = TH2F(histname, ";P_{T};#eta",nPt, ptArr,nEta, etaArr)
    for x in range(1,7):
        for y in range(1,12):
            content = NominalHist.GetBinContent(x,y)
            outputhist.SetBinContent(y,x,content)
    return outputhist

def Syst(File, histname,reverse = False):
     
    TruthHist = File.Get("SystFSR").Clone();
    TruthHist.Scale(0)
    SidebandHist = File.Get("SystSideband").Clone();
    FSRHist = File.Get("SystFSR").Clone();    
    outputhist = TH2F(histname, ";P_{T};#eta",nPt, ptArr,nEta, etaArr)
    for x in range(1,7):
        for y in range(1,5):
            content = math.sqrt(TruthHist.GetBinContent(x,y)*TruthHist.GetBinContent(x,y)+SidebandHist.GetBinContent(x,y)*SidebandHist.GetBinContent(x,y)+FSRHist.GetBinContent(x,y)*FSRHist.GetBinContent(x,y))
            if not reverse:
               outputhist.SetBinContent(y,x,content)
            else:
               outputhist.SetBinContent(y,x,-1.0*content)
    return outputhist

def Stat(File, histname,reverse = False):
    StatHist = File.Get("StatError").Clone();
    outputhist = TH2F(histname, ";P_{T};#eta",nPt, ptArr,nEta, etaArr)
    for x in range(1,7):
        for y in range(1,5):
            content = StatHist.GetBinContent(x,y)
            if not reverse:
               outputhist.SetBinContent(y,x,content)
            else:
               outputhist.SetBinContent(y,x,-1.0*content)

    return outputhist





def main(argv):
   WP = str(argv[0])
   WPF= str(argv[1])
   WFile1516 = TFile("SF_Wrong_1516_"+WP+".root","READ")
#   RFile1516 = TFile("SF1516/SF_Right_20152016_"+WP+".root","READ")
#   WFile17 = TFile("SF17/SF_Wrong_2017_"+WP+".root","READ")
#   RFile17 = TFile("SF17/SF_Right_2017_"+WP+".root","READ")
   Output = TFile("output/ChargeCorrectionSF."+WPF+".root","recreate")
   
   SFCentral_RunNumber276262_311481_SS = Nominal(WFile1516,"SFCentral_RunNumber276262_311481_SS").Clone();SFCentral_RunNumber276262_311481_SS.Write();
   STAT_RunNumber276262_311481_SS = Stat(WFile1516,"STAT_RunNumber276262_311481_SS",True).Clone();STAT_RunNumber276262_311481_SS.Write();
   SYSTotal_RunNumber276262_311481_SS  = Syst(WFile1516,"SYSTtotal_RunNumber276262_311481_SS",True).Clone();SYSTotal_RunNumber276262_311481_SS.Write();
   
#   SFCentral_RunNumber276262_311481_OS = Nominal(RFile1516,"SFCentral_RunNumber276262_311481_OS").Clone();SFCentral_RunNumber276262_311481_OS.Write();
#   STAT_RunNumber276262_311481_OS = Stat(RFile1516,"STAT_RunNumber276262_311481_OS").Clone();STAT_RunNumber276262_311481_OS.Write();
#   SYSTotal_RunNumber276262_311481_OS  = Syst(RFile1516,"SYSTtotal_RunNumber276262_311481_OS").Clone();SYSTotal_RunNumber276262_311481_OS.Write();
#
#
#   SFCentral_RunNumber325713_340453_SS = Nominal(WFile17,"SFCentral_RunNumber325713_340453_SS").Clone();SFCentral_RunNumber325713_340453_SS.Write()
#   STAT_RunNumber325713_340453_SS = Stat(WFile17,"STAT_RunNumber325713_340453_SS",True).Clone();STAT_RunNumber325713_340453_SS.Write()
#   SYSTotal_RunNumber325713_340453_SS = Syst(WFile17,"SYSTtotal_RunNumber325713_340453_SS",True).Clone();SYSTotal_RunNumber325713_340453_SS.Write()
#
#   SFCentral_RunNumber325713_340453_OS = Nominal(RFile17,"SFCentral_RunNumber325713_340453_OS").Clone();SFCentral_RunNumber325713_340453_OS.Write()
#   STAT_RunNumber325713_340453_OS = Stat(RFile17,"STAT_RunNumber325713_340453_OS").Clone();STAT_RunNumber325713_340453_OS.Write()
#   SYSTotal_RunNumber325713_340453_OS = Syst(RFile17,"SYSTtotal_RunNumber325713_340453_OS").Clone();SYSTotal_RunNumber325713_340453_OS.Write()
   
   return
main(sys.argv[1:])
