1. Run ExtractDATA(MC or FSR).py on samples coming from T&P SkimToNtuple option, to get 2D histo for llh
2. Run createMatrix.py to get likelihood. python [DATA/MC] [Period] [WorkingPoint] [Syst]
3. ExtractScaleFactor.py contains some plotting function.
4. ToRecommendation.py just change the format of eff/sf to recommendation style. python ToRecommendation.py [WorkingPoint] [output WorkingPoint Name] 
5. MassDATA(MC or FSR).py are not used for any result but just for checking the background subtraction.
6. Be carefull about data llh after background subtraction and MC llh. Especially in tight requirement due to the input matrix becomes sparse, some time charge flip rate likelihood calculation will reach the limit.
