from ROOT import *
#from ROOT import TH2F, TH1F, TFile,TTree
from array import array
import math
import numpy as np
import sys

#pts  =  [20,30,35,40,45,60,95,115, 145,185,240, 7000]
#pts  = [20.0,60.0,90.0, 7000.0]
#ets  = [20.0,60.0,90.0,130.0,7000.0]
#etas = [0.0, 0.6,1.1, 1.52, 1.7, 2.3, 2.5]
#etas = [0.0, 0.75, 1.52, 1.7, 2.3, 2.5]

pts  = [20,60,90,7000]
etas = [0.0, 0.75, 1.52, 1.7, 2.5]

def ApplyPreCuts(tree):
   if not tree.isTagTag==1:
      return False
   if not tree.isMultiElectronTriggerMatched:
      return False
   if tree.elCand1_pt<20000. or tree.elCand2_pt<20000.:
      return False
   if math.fabs(tree.elCand1_cl_eta)>1.37 and math.fabs(tree.elCand1_cl_eta)<1.52:
      return False
   if math.fabs(tree.elCand2_cl_eta)>1.37 and math.fabs(tree.elCand2_cl_eta)<1.52:
      return False
   if not tree.elCand2_FirstEgMotherOrigin==13:
      return False
   if not tree.elCand1_FirstEgMotherOrigin==13:
      return False
   if not tree.elCand2_FirstEgMotherTyp==2:
      return False
   if not tree.elCand1_FirstEgMotherTyp==2:
      return False
   if math.fabs(tree.elCand2_cl_eta)>2.47 or math.fabs(tree.elCand1_cl_eta)>2.47:
      return False
   if tree.Zcand_M<60000. or tree.Zcand_M>120000.:
     return False
   return True

def ApplyWPcutsTightT(tree):
    if tree.elCand1_FixedCutTight==0 or tree.elCand2_FixedCutTight==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
def ApplyWPcutsTightTCFT(tree):
    if tree.elCand1_FixedCutTight==0 or tree.elCand2_FixedCutTight==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_passCFTloose==0 or tree.elCand2_passCFTloose==0:
      return False
    else:
      return True


def ApplyWPcutsTightT(tree):
    if tree.elCand1_FixedCutTight==0 or tree.elCand2_FixedCutTight==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
def ApplyWPcutsTightGCFT(tree):
    if tree.elCand1_isolGradient==0 or tree.elCand2_isolGradient==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_passCFTloose==0 or tree.elCand2_passCFTloose==0:
      return False
    else:
      return True
def ApplyWPcutsTight(tree):
    if tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True

def ApplyWPcutsTightL(tree):
    if tree.elCand1_FixedCutLoose==0 or tree.elCand2_FixedCutLoose==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
def ApplyWPcutsTightG(tree):
    if tree.elCand1_isolGradient==0 or tree.elCand2_isolGradient==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
	  
def ApplyWPcutsTightH(tree):
    if tree.elCand1_FixedCutHighPtCaloOnly==0 or tree.elCand2_FixedCutHighPtCaloOnly==0 or tree.elCand2_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_TightLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
	  
def ApplyWPcutsMediumT(tree):
    if tree.elCand1_FixedCutTight==0 or tree.elCand2_FixedCutTight==0 or tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
def ApplyWPcutsMediumTCFT(tree):
    if tree.elCand1_FixedCutTight==0 or tree.elCand2_FixedCutTight==0 or tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_passCFTloose==0 or tree.elCand2_passCFTloose==0:
      return False
    else:
      return True

def ApplyWPcutsMediumGCFT(tree):
    if tree.elCand1_isolGradient==0 or tree.elCand2_isolGradient==0 or tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_passCFTloose==0 or tree.elCand2_passCFTloose==0:
      return False
    else:
      return True
def ApplyWPcutsMediumG(tree):
    if tree.elCand1_isolGradient==0 or tree.elCand2_isolGradient==0 or tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True
	  
def ApplyWPcutsMedium(tree):
    if tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True

def ApplyWPcutsMediumL(tree):
    if tree.elCand1_FixedCutLoose==0 or tree.elCand2_FixedCutLoose==0 or tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True

def ApplyWPcutsMediumH(tree):
    if tree.elCand1_FixedCutHighPtCaloOnly==0 or tree.elCand2_FixedCutHighPtCaloOnly==0 or tree.elCand2_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_MediumLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True	  

def ApplyWPcutsLooseBL(tree):
    if tree.elCand2_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0:
      return False
    else:
      return True	  
	  
def ApplyWPcutsLooseBLCFT(tree):
    if tree.elCand2_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest==0 or tree.elCand1_passCFTloose==0 or tree.elCand2_passCFTloose==0:
      return False
    else:
      return True

def ApplyWPcuts(tree,WP): 
        if WP in ["Tight"]: 
	   return ApplyWPcutsTight(tree)
        elif WP in ["CFTTightG"]:
           return ApplyWPcutsTightGCFT(tree)
	elif WP in ["TightT"]:
           return ApplyWPcutsTightT(tree)
	elif WP in ["CFTTightT"]:   
	   return ApplyWPcutsTightTCFT(tree)
	elif WP in ["TightG"]:   
	   return ApplyWPcutsTightG(tree)
	elif WP in ["TightC"]:   
	   return ApplyWPcutsTightH(tree)
	elif WP in ["TightL"]:   
	   return ApplyWPcutsTightL(tree)
	elif WP in ["Medium"]: 
	   return ApplyWPcutsMedium(tree)
	elif WP in ["MediumT"]:   
	   return ApplyWPcutsMediumT(tree)
	elif WP in ["CFTMediumT"]:   
	   return ApplyWPcutsMediumTCFT(tree)
	elif WP in ["MediumG"]:   
	   return ApplyWPcutsMediumG(tree)
	elif WP in ["MediumC"]:   
	   return ApplyWPcutsMediumH(tree)
	elif WP in ["MediumL"]:   
	   return ApplyWPcutsMediumL(tree)
	elif WP in ["CFTMediumG"]:   
	   return ApplyWPcutsMediumGCFT(tree)
	elif WP in ["LooseB"]:   
	   return ApplyWPcutsLooseBL(tree)
	elif WP in ["CFTLooseB"]:   
	   return ApplyWPcutsLooseBLCFT(tree)   
	else:
	   return False


def FindEtaBin(eta):
    for index in range(0,len(etas)):
        if abs(eta) < etas[index]:
           return int(index)-1
def FindPtBin(pt):
    for index in range(0,len(pts)):
        if pt/1000. < pts[index]:
           return int(index)-1




	   
def main(argv):
    print "python ExtractTruth.py period job_number"
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    gROOT.SetBatch(1)
    gStyle.SetLineWidth(1)
    gStyle.SetMarkerSize(1)
    gStyle.SetOptStat(False)
    gStyle.SetPaintTextFormat("1.4f");
    gStyle.SetErrorX(0)


    #pts  = [20.0,30.0,40.0,50.0,60.0,70.0, 95.0, 130.0, 1000.0]
    #pts  = [20.0,30.0,35.0,40.0,45.0,60.0,95.0,115, 145, 185, 240,7000.0]

    Period = argv[0]
    inputpath = "./mc"+str(Period)+".root"
    if Period == "1516":
       inputpath = "/lustre/stormfs/atlas/atlaslocalgroupdisk/rucio/user/haxu/42/8f/mc1516.root" 
    if Period == "17":
       inputpath = "/lustre/stormfs/atlas/atlaslocalgroupdisk/rucio/user/haxu/67/8b/mc17.root"
    ZeeMC = TFile(str(inputpath),"READ")
    ZeeTree = ZeeMC.Get("ZeeCandidate").Clone()
   
  
    nEta=len(etas) - 1
    nPt = len(pts) - 1
    total_bins = nEta*nPt
    Output = TFile("./M_MC"+str(Period)+"_"+str(argv[1])+".root","recreate")
    Nentry = ZeeTree.GetEntries()
    #WorkingPoints=["TightG","TightC","TightT","TightL","Tight","MediumG","MediumC","MediumT","MediumL","Medium","Loose"]
#    WorkingPoints=["CFTTightG","CFTMediumG","CFTMediumT","CFTTightT","CFTLooseB","MediumG","TightT","Tight"]
    WorkingPoints=["TightG","TightC","TightT","TightL","Tight","MediumG","MediumC","MediumT","MediumL","Medium","Loose","CFTTightG","CFTMediumG","CFTMediumT","CFTTightT","CFTLooseB"]
    MassSS = {}
    MassOS = {}
    for index_wp in range(0,16):
            print WorkingPoints[index_wp]
            Output.mkdir(WorkingPoints[index_wp])
            
            for  el1_pt in range(0,nPt):
              for  el1_eta in range(0,nEta):
                 for  el2_pt in range(0,nPt):
                    for  el2_eta in range(0,nEta):
                       #print pts[el1_pt],etas[el1_eta],pts[el2_pt],etas[el2_eta]
                        region = str(WorkingPoints[index_wp])+"_el1_pt_"+str(pts[el1_pt])+"_eta_"+str(etas[el1_eta])+"_el2_pt_"+str(pts[el2_pt])+"_eta_"+str(etas[el2_eta])
                        MassSS[region] = TH1F("MassSS_"+region,";Mll;Events",60,60,120)
                        MassOS[region]  = TH1F("MassOS_"+region,";Mll;Events",60,60,120)
#            MinusHistTotal.append(TH2F(str(WorkingPoints[index_wp])+"_minus_Total",";#eta;P_{T}",nEta, etaArr, nPt, ptArr))
#    print MassSS
#    return
    Start = Nentry*int(argv[1])/100
    End = Nentry*int(int(argv[1])+1)/100
    for index in range(Start,End):
        #continue;
        ZeeTree.GetEntry(index)
        if ZeeTree.Zcand_M>80000. and ZeeTree.Zcand_M<100000.:
           MCPileupWeight  = 1#eeTree.MCPileupWeight
        elif ZeeTree.Zcand_M>60000. and ZeeTree.Zcand_M<120000.:
           MCPileupWeight  = 1 #0.50#-0.5*ZeeTree.MCPileupWeight
        if index%100000 == 0:
           print index 
        if not ApplyPreCuts(ZeeTree):
           continue  
        for index_wp in range(0,16):
	    if not ApplyWPcuts(ZeeTree,str(WorkingPoints[index_wp])):
		   continue;
            
            eta_el1 = int(FindEtaBin(ZeeTree.elCand1_cl_eta))
            pt_el1 = int(FindPtBin(ZeeTree.elCand1_pt))
            eta_el2 = int(FindEtaBin(ZeeTree.elCand2_cl_eta))
            pt_el2 =  int(FindPtBin(ZeeTree.elCand2_pt))
      #      print ZeeTree.elCand1_cl_eta,etas[eta_el1],ZeeTree.elCand1_pt,pts[pt_el1]
            field = str(WorkingPoints[index_wp])+"_el1_pt_"+str(pts[pt_el1])+"_eta_"+str(etas[eta_el1])+"_el2_pt_"+str(pts[pt_el2])+"_eta_"+str(etas[eta_el2])
            if ZeeTree.elCand1_charge * ZeeTree.elCand2_charge > 0 :
               MassSS[field].Fill(ZeeTree.Zcand_M/1000.,ZeeTree.MCPileupWeight)
            else:
               MassOS[field].Fill(ZeeTree.Zcand_M/1000.,ZeeTree.MCPileupWeight) 
               

  

    Output.cd()
    for index_wp in range(0,16):
        Output.cd(WorkingPoints[index_wp])
        for  el1_pt in range(0,nPt):
              for  el1_eta in range(0,nEta):
                 for  el2_pt in range(0,nPt):
                    for  el2_eta in range(0,nEta):
                       #print pts[el1_pt],etas[el1_eta],pts[el2_pt],etas[el2_eta]
                        region = str(WorkingPoints[index_wp])+"_el1_pt_"+str(pts[el1_pt])+"_eta_"+str(etas[el1_eta])+"_el2_pt_"+str(pts[el2_pt])+"_eta_"+str(etas[el2_eta])
                        MassSS[region].Write(); #= TH1F("MassSS_"+region,";Mll;Events",60,60,120)
                        MassOS[region].Write(); # = TH1F("MassOS_"+region,";Mll;Events",60,60,120) 

       
       
       
       
       
       

   #     MinusHistTotal[index_wp].Write()
    #Output.Write()
    #Output.Close()	
    return
main(sys.argv[1:])


