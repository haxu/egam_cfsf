from ROOT import TMinuit, Long, Double,TFile,TH2F
from math import log as log
import numpy as np
from numpy.linalg import inv
import time
#import sys
from array import array
import rutil

start_time = time.time()
def logLlhPair(i, j, parameters):
  nIJ = n[i,j]
  nssIJ = nss[i,j]
  bgssIJ = bgss[i,j]
  epsI, epsJ = parameters[i], parameters[j]
  expectedNss = nIJ * ((1-epsI)*epsJ + (1-epsJ)*epsI)+bgssIJ
  try:
    logLlh = nssIJ * log(expectedNss) - expectedNss
  except ValueError:
    logLlh = 0.0
  return logLlh

def negativeLog(numParameters, parameters):
  negLog = 0.0
  i = 0
  while i < numParameters[0]:
    j = 0
    while j < numParameters[0]:
      negLog -= logLlhPair(i, j, parameters)
      j += 1
    i += 1
  return negLog

def fcn(numParameters, derivatives, function, parameters, internalFlag):
  function[0] = negativeLog(numParameters, parameters)

def ratesAndErrors(numParameters, minuit):
  rates = np.zeros(numParameters)
  rateErrors = np.zeros(numParameters)
  rate, rateError = Double(0), Double(0)
  for i in xrange(numParameters):
    minuit.GetParameter(i, rate, rateError)
    rates[i] = float(rate)
    rateErrors[i] = float(rateError)
  # correlation matrix
  covMat = np.zeros(numParameters*numParameters)
  #print covMat
  minuit.mnemat(covMat,numParameters)
  covMat = covMat.reshape(numParameters,numParameters)
  #print covMat
  #print covMat.size()
  D = np.sqrt(np.diag(np.diag(covMat)))
  DInv = inv(D)
  corrMat = DInv.dot(covMat.dot(DInv))
  print corrMat
  return rates, rateErrors, covMat


def runMinuit(numParameters):
  minuit = TMinuit(numParameters)
  minuit.SetPrintLevel(0)

  minuit.SetFCN(fcn)
  arglist = np.zeros(numParameters) + 0.01
  internalFlag, arglist[0] = Long(0), 0.5
  minuit.mnexcm("SET ERR", arglist, 1, internalFlag)

  initialValues = np.zeros(numParameters) + 0.001
  steps = np.zeros(numParameters) + 0.00001

  for i in xrange(numParameters):
    name = "epsilon%s" % i
    minuit.mnparm(i, name, initialValues[i], steps[i], 0.00000, 0.2, internalFlag)

  # arglist[0] = 2
  # minuit.mnexcm("SET STR", arglist, 1, internalFlag)

  arglist[0], arglist[1] = 10000, 0.1
  minuit.mnexcm("SIMPLEX", arglist, 1, internalFlag)
  minuit.mnexcm("MIGRAD", arglist, 1, internalFlag)

  print "FIT STATUS is " +str(minuit.GetStatus())
  return ratesAndErrors(numParameters, minuit)




def createMatrix(hist_2d,zero=False):
    ListCount=[]
    print hist_2d.GetNbinsX() 
    number = hist_2d.GetNbinsX()+2
    i=0
    for binx in range(2,number):
        ListCount2=[]
        for biny in range(2,number):
            content = hist_2d.GetBinContent(binx,biny)
            if content < 0 :
               #print binx,biny,"WARNING!!!!!"
               content = 0.0000000
            if content > 0 :
             if biny==1 :
               i = i+1
            if not zero:
               ListCount2.append(content)
            else:
               ListCount2.append(0)
        ListCount.append(ListCount2)
         
    output = np.array(ListCount)
    print hist_2d.GetName(),"rank",np.linalg.matrix_rank(output),"det",np.linalg.det(output)#ListCount[36][48],output[36,48]
    return output#ListCount

##############################


pts  = [20.0,60.0,90.0, 130,7000.0]
etas = [0.0, 0.6, 1.52, 1.7, 2.3, 2.5]




##############################
nEta,nPt = len(etas),len(pts)
numParameters = (nEta-1)*(nPt-1)
def doIt(n, nss,bgss, name, outFile):
  rates,errors,corrMat = runMinuit(numParameters)
  #ratesN, errorsN = '%s_rates.txt' % name, '%s_errors.txt' % name ##### Can also be without ".txt" at the not to save as txt
  #with open(ratesN, 'wb') as ratesF, open(errorsN, 'wb') as errorsF:
  #  np.savetxt(ratesF, rates)   ##### if no ".txt" is given above, here use just save()
  #  np.savetxt(errorsF, errors) ##### if no ".txt" is given above, here use just save()
  #  with open(outFile, 'a') as txtOut:
  #    txtOut.write('%s\n%s\n' % (ratesN, errorsN))
  reLabels = rutil.ratesErrorsBins(rates, errors, etas, pts)
  rutil.printRatesErrorsBins(name, reLabels)
  corrMatHisto = rutil.npToTH2(corrMat, etas, pts, '%s_corrMat' % name)
#  rutil.writeToFileU('%s.root' % name, corrMatHisto)
##### This part was added for TH2 plotting
  print name
  #bName = rutil.beautifyName(name)
  rutil.writeToFile('%s.root' % name, rutil.hist2D(rates, errors, etas, pts, '%s_misid' % name))
  rutil.writeToFileU('%s.root' % name, corrMatHisto)
#  rutil.writeToFileU('%s.root' % name, rutil.Count2D(n,etas, pts, '%s_N' % Name))

if __name__ == '__main__':
    import sys
    Sample = sys.argv[1]
    Input = sys.argv[2]
    WorkingPoints = sys.argv[3]
    Syst = sys.argv[4]
    FileDATA = TFile("Data"+str(Input)+".root","READ")
    FileMC = TFile("MC"+str(Input)+".root","READ")
    FileFSR = TFile("FSR"+str(Input)+".root","READ")
    hist_2d_as = "";
    hist_2d_ss = "";
    hist_2d_bkgss = "";
    if Syst in ['nominal']:
       if Sample in ["MC"]:
         hist_2d_as_main = FileMC.Get(str(WorkingPoints)+"_total")
         #hist_2d_as_sideband = FileMC.Get("AS_"+str(WorkingPoints)+"_Sideband_2D")
         hist_2d_ss_main = FileMC.Get(str(WorkingPoints)+"_samesign")
        # hist_2d_ss_sideband = FileMC.Get("SS_"+str(WorkingPoints)+"_Sideband_2D")
         #hist_2d_as_main.Add(hist_2d_as_sideband,-1.);
         hist_2d_as = hist_2d_as_main.Clone();
         #hist_2d_ss_main.Add(hist_2d_ss_sideband,-1.);
         hist_2d_ss = hist_2d_ss_main.Clone();
         hist_2d_bkgss = hist_2d_ss.Clone();
         hist_2d_bkgss.Scale(0)
         #print hist_2d_ss_main.Integral(),hist_2d_ss_main.GetBinContent(1,1),hist_2d_ss_main.GetBinContent(1,2),hist_2d_ss_main.GetBinContent(2,1),"TEXT"
         #hist_2d_ss = hist_2d_ss_main.Clone();hist_2d_bkgss = hist_2d_ss_sideband.Clone();hist_2d_bkgss.Scale(0.5);
       elif Sample in ['DATA']:
         hist_2d_as_main = FileDATA.Get(str(WorkingPoints)+"_total")
         hist_2d_as_sideband = FileDATA.Get(str(WorkingPoints)+"_total_SB")
         print hist_2d_as_sideband.Integral(),"hist_2d_as_sideband"
         hist_2d_ss_main = FileDATA.Get(str(WorkingPoints)+"_samesign")
         hist_2d_ss_sideband = FileDATA.Get(str(WorkingPoints)+"_samesign_SB")
         print hist_2d_ss_sideband.Integral(),"hist_2d_ss_sideband"
         hist_2d_as_mc = FileMC.Get(str(WorkingPoints)+"_total")
         hist_2d_ss_mc = FileMC.Get(str(WorkingPoints)+"_samesign")
   
         hist_2d_as_sideband_mc = FileMC.Get(str(WorkingPoints)+"_total_SB")
         hist_2d_ss_sideband_mc = FileMC.Get(str(WorkingPoints)+"_samesign_SB")
   
         hist_2d_as_sideband.Add(hist_2d_as_sideband_mc,-1.)
#         hist_2d_ss_sideband.Add(hist_2d_ss_sideband_mc,-1.)

         hist_2d_as_main_FSR = FileFSR.Get(str(WorkingPoints)+"_total")
         hist_2d_ss_main_FSR = FileFSR.Get(str(WorkingPoints)+"_samesign")
         
         hist_2d_as_sideband_fsr = FileFSR.Get(str(WorkingPoints)+"_total_SB")
         hist_2d_ss_sideband_fsr = FileFSR.Get(str(WorkingPoints)+"_samesign_SB")
         
         hist_2d_as_sideband.Add(hist_2d_as_sideband_fsr,-1)
 #        hist_2d_ss_sideband.Add(hist_2d_ss_sideband_fsr,-1)



         hist_2d_as_main.Add(hist_2d_as_sideband,-1.0);hist_2d_as_main.Add(hist_2d_as_main_FSR,-1.0);
         hist_2d_as = hist_2d_as_main.Clone();
         hist_2d_ss = hist_2d_ss_main.Clone();
#         print hist_2d_ss.Integral(),0.88*hist_2d_ss_mc.Integral(),0.88*hist_2d_ss_main_FSR.Integral(),"1111111"
#         print hist_2d_as.Integral(),0.88*hist_2d_as_mc.Integral(),0.88*hist_2d_as_main_FSR.Integral(),"3333333"
#         print hist_2d_ss_sideband.Integral()-0.88*hist_2d_ss_sideband_mc.Integral()-hist_2d_ss_sideband_fsr.Integral()
#         print hist_2d_as_sideband.Integral()-0.88*hist_2d_as_sideband_mc.Integral()-hist_2d_as_sideband_fsr.Integral()
          
    #     hist_2d_ss.Add(hist_2d_ss_sideband,-0.0)
         #hist_2d_ss.Scale(10./17.)  
         hist_2d_ss.Add(hist_2d_ss_sideband,-1.0)

         
         hist_2d_bkgss = hist_2d_ss_main_FSR.Clone();
         #hist_2d_ss.Add(hist_2d_ss_main_FSR,-1.0)
         #print hist_2d_ss.Integral(),"222222"
         #for i in range(1,25):
         #    for j in range(1,25):
         #       if not hist_2d_ss_mc.GetBinContent(i,j)==0 :
         #          print hist_2d_ss.GetBinContent(i,j)/hist_2d_ss_mc.GetBinContent(i,j),
         #       else: 
         #          print 0.0,
         #    print ""
         hist_2d_bkgss.Scale(0)
    elif Syst in ['Sideband']:
        if Sample in ["MC"]:
         hist_2d_as = FileMC.Get(str(WorkingPoints)+"_total")
         hist_2d_ss = FileMC.Get(str(WorkingPoints)+"_samesign")
         hist_2d_bkgss = hist_2d_ss.Clone();
         hist_2d_bkgss.Scale(0)
         print hist_2d_bkgss.Integral() 
        elif Sample in ['DATA']:
         hist_2d_as_main = FileDATA.Get(str(WorkingPoints)+"_total")
         hist_2d_ss_main = FileDATA.Get(str(WorkingPoints)+"_samesign")  
         hist_2d_as_main_FSR = FileFSR.Get(str(WorkingPoints)+"_total")
         hist_2d_ss_main_FSR = FileFSR.Get(str(WorkingPoints)+"_samesign")
         hist_2d_as_main.Add(hist_2d_as_main_FSR,-1.);hist_2d_as = hist_2d_as_main.Clone();
         #hist_2d_ss_main.Add(hist_2d_ss_main_FSR,-1.);
         hist_2d_ss = hist_2d_ss_main.Clone();
         hist_2d_bkgss = hist_2d_ss_main_FSR.Clone();
         hist_2d_ss.Add(hist_2d_ss_main_FSR,-1.)
         hist_2d_bkgss.Scale(0)
    elif Syst in ['FSR']:
        if Sample in ["MC"]:
         hist_2d_as_main = FileMC.Get(str(WorkingPoints)+"_total")
         hist_2d_ss_main = FileMC.Get(str(WorkingPoints)+"_samesign")
         #hist_2d_as_sideband = FileMC.Get("AS_"+str(WorkingPoints)+"_Sideband_2D")
         #hist_2d_ss_sideband = FileMC.Get("SS_"+str(WorkingPoints)+"_Sideband_2D")
         #hist_2d_as_main_FSR = FileMC.Get("AS_"+str(WorkingPoints)+"_Main_FSR_2D")
         #hist_2d_ss_main_FSR = FileMC.Get("SS_"+str(WorkingPoints)+"_Main_FSR_2D")
         #hist_2d_as_sideband_FSR = FileMC.Get("AS_"+str(WorkingPoints)+"_Sideband_FSR_2D")
        # hist_2d_ss_sideband_FSR = FileMC.Get("SS_"+str(WorkingPoints)+"_Sideband_FSR_2D")
        # hist_2d_as_main.Add(hist_2d_as_main_FSR);hist_2d_as_sideband.Add(hist_2d_as_sideband_FSR);hist_2d_as_main.Add(hist_2d_as_sideband,-1.);
         hist_2d_as = hist_2d_as_main.Clone();
        # hist_2d_ss_main.Add(hist_2d_ss_main_FSR);hist_2d_ss_sideband.Add(hist_2d_ss_sideband_FSR);hist_2d_ss_main.Add(hist_2d_ss_sideband,-1.);
         hist_2d_ss = hist_2d_ss_main.Clone();
         hist_2d_bkgss = hist_2d_ss_main.Clone();
         hist_2d_bkgss.Scale(0);
        elif Sample in ['DATA']:
         hist_2d_as_main = FileDATA.Get(str(WorkingPoints)+"_total")
         hist_2d_as_sideband = FileDATA.Get(str(WorkingPoints)+"_total_SB")
         hist_2d_ss_main = FileDATA.Get(str(WorkingPoints)+"_samesign")
         hist_2d_ss_sideband = FileDATA.Get(str(WorkingPoints)+"_samesign_SB")
         hist_2d_as_main.Add(hist_2d_as_sideband,-1.);hist_2d_as = hist_2d_as_main.Clone();

         hist_2d_bkgss = hist_2d_ss_sideband.Clone();
         #hist_2d_bkgss.Scale(0.5);
         hist_2d_ss = hist_2d_ss_main.Clone();
         hist_2d_ss.Add(hist_2d_ss_sideband,-1.)
         hist_2d_bkgss.Scale(0)
    else:
         print "ERROR!!!"

    n =  createMatrix(hist_2d_as)
    nss = createMatrix(hist_2d_ss)
    np.set_printoptions(threshold=np.inf)
    #print "N",n
    #print "Nss",nss
    bgss = createMatrix(hist_2d_bkgss)
    doIt(n, nss,bgss,str(Sample)+str(Input)+"_"+str(WorkingPoints)+"_"+str(Syst),str(Sample)+ str(Input)+"_"+str(WorkingPoints)+"_"+str(Syst))
print("--- %s TIME ---" % (time.time() - start_time))

